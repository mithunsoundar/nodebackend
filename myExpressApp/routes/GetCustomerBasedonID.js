
// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());
const connection = require('./db');




//Customer Api Get Customer based on customer ID = 111
router.get('/bank/api/customer/:id', (req, res) => {
    const CustomerID = parseInt(req.params.id);
    console.log(CustomerID)
    //res.send(CustomerID.toString())
    GetCustomerBasedonID(CustomerID.toString(), function(err, result) {
      if (err) {
        console.error("Error:", err);
        res.status(500).send("Internal Server Error");
      } else {
        console.log("Result:", JSON.stringify(result, null, 2));
        // Send the JSON result as a response
        res.send(result);
      }
    });
  
    
  });
  function GetCustomerBasedonID(CustomerID,callback) {

    let query = util.format('SELECT * from customers Where customer_id = %s', CustomerID);
  
    var request = new Request(query, function(err) {
      if (err) {
        console.log(err);
        callback(err, null);
      }
    });
  
    var resultArray = [];
  
    request.on('row', function(columns) {
      var rowObject = {};
      columns.forEach(function(column) {
        if (column.value === null) {
          rowObject[column.metadata.colName] = null;
        } else {
          rowObject[column.metadata.colName] = column.value;
        }
      });
      resultArray.push(rowObject);
    });
  
    // Close the connection after the final event emitted by the request, after the callback passes
    request.on("requestCompleted", function(rowCount, more) {
      //connection.close();
      callback(null, resultArray);
    });
  
    connection.execSql(request);
  }
  module.exports = router;