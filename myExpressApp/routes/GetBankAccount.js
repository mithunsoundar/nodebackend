// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());
const connection = require('./db');



//BankAccount Api
// GET route to get bank account data by ID
router.get('/bank/api/bankaccount', (req, res) => {
    console.log("Default rount for bankaccount Success")
    GetBankAccount((err, result) => {
      if (err) {
        console.error("Error:", err);
        res.status(500).send("Internal Server Error");
      } else {
        console.log("Result:", JSON.stringify(result, null, 2));
        // Send the JSON result as a response
        res.send(result);
      }
    });  
    
  });
  function GetBankAccount(callback) {
    var request = new Request("Select * from BankAccount ", function(err) {
      if (err) {
        console.log(err);
   
        callback(err, null);
      }
    });
  
    var resultArray = [];
  
    request.on('row', function(columns) {
      var rowObject = {};
      columns.forEach(function(column) {
        if (column.value === null) {
          rowObject[column.metadata.colName] = null;
        } else {
          rowObject[column.metadata.colName] = column.value;
        }
      });
      resultArray.push(rowObject);
    });
  
    // Close the connection after the final event emitted by the request, after the callback passes
    request.on("requestCompleted", function(rowCount, more) {
      //connection.close();
      callback(null, resultArray);
    });
  
    connection.execSql(request);
  }

  module.exports = router;
