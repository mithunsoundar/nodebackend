// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());
const connection = require('./db');

router.post('/bank/api/bankaccount/transfer', (req, res) => {
    var transferDetails = req.body;  
    transferMoney(transferDetails.fromAccount, transferDetails.toAccount, transferDetails.amount, function(err, result) {
      if (err) {
        console.error("Error transferring money:", err);
        res.status(500).send("Error transferring money");
      } else {
        console.log("Money transferred successfully:", result);
        // Respond once the transaction is completed
        res.send("Transfer completed");
      }
    });
  });

  function transferMoney(fromAccount, toAccount, amount, callback) {
    withdrawMoney(fromAccount, amount, function(err, withdrawResult) {
      if (err) {
        callback(err);
      } 
      else
      {
      // Withdrawal successful, now deposit to the destination account
      depositMoney(toAccount, amount, function(err, depositResult) {
        if (err) {
          // If deposit fails, refund the withdrawn amount to the source account
          refundMoney(fromAccount, amount, function(refundErr) {
            if (refundErr) {
              console.error("Error refunding money:", refundErr);
            }
            callback(err || refundErr); // Return deposit error if any, or refund error, or null
          });
        } else {
          callback(null, "Transfer successful");
        }
      });
      }
    });
  }

  function withdrawMoney(customerName, amountToWithdraw, callback) {
    // call UpdateTransaction Method 
    UpdateBankAccountTablewithdraw(customerName, amountToWithdraw, function(err, rowCount, rows) {
      if (err) {
        console.error('Error:', err);
        callback(err); // Pass the error to the callback
      } else {
        console.log('Rows affected:', rowCount);
        console.log('Selected rows:', rows);
        callback(null, "Transaction successful"); // Pass null for error (indicating success)
      }
    });
  }

  function depositMoney(customerName, amountToDeposit, callback) {
    // Your deposit logic goes here
    // Example: Implement similar to withdrawMoney but with a positive balance update
    UpdateBankAccountTableDeposit(customerName, amountToDeposit, function(err, rowCount, rows) {
      if (err) {
        console.error('Error:', err);
        callback(err); // Pass the error to the callback
      } else {
        console.log('Rows affected:', rowCount);
        console.log('Selected rows:', rows);
        callback(null, "Transaction successful"); // Pass null for error (indicating success)
      }
    });
  }
  function UpdateBankAccountTableDeposit(customerName, amountToDeposit, callback)
  {
    const sql = util.format(
      "UPDATE BankAccount SET balance = balance + %d WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = '%s')",
      amountToDeposit,
      customerName    
    );
    var request = new Request(sql, function(err, result) {
      // Handle the callback logic here
      callback(err, result);
    });
        // Replace it with the appropriate code for your database library
    request.on('row', function(columns) {
    console.log("selected  successfully");
    });
    
    request.on('rowsaffected', rowCount => {
    console.log("inserted successfully")
    });
    connection.execSql(request);
  } 

  function UpdateBankAccountTablewithdraw(customerName, amountToWithdraw, callback)
{
  const sql = util.format(
    "UPDATE BankAccount SET balance = balance - %d WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = '%s')",
    amountToWithdraw,
    customerName    
  );
  var request = new Request(sql, function(err, result) {
    // Handle the callback logic here
    callback(err, result);
  });
      // Replace it with the appropriate code for your database library
  request.on('row', function(columns) {
  console.log("selected  successfully");
  });
  
  request.on('rowsaffected', rowCount => {
  console.log("inserted successfully")
  });
  connection.execSql(request);
}
  function refundMoney(customerName, amountToRefund, callback) {
    // Your refund logic goes here
    // Example: Implement similar to depositMoney but for the source account
    UpdateBankAccountTableDeposit(customerName, amountToRefund, function(err, rowCount, rows) {
      if (err) {
        console.error('Error:', err);
        callback(err); // Pass the error to the callback
      } else {
        console.log('Rows affected:', rowCount);
        console.log('Selected rows:', rows);
        callback(null, "Transaction successful"); // Pass null for error (indicatng success)
      }
    });
  
  }

  module.exports = router;