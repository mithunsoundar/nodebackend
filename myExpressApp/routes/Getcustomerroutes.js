// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());

const connection = require('./db');

//Get Customer Test
router.get('/bank/api/customers/test', (req, res) => {
    console.log("Get Customer TEst ")
  });

  
//Customers ApI Get method
router.get('/bank/api/customers', (req, res) => {
 
    GetCustomers((err, result) => {
      if (err) {
        console.error("Error:", err);
        res.status(500).send("Internal Server Error");
      } else {
        console.log("Result:", JSON.stringify(result, null, 2));
        // Send the JSON result as a response
        res.send(result);
      }
    });  
  }); 

function GetCustomers(callback) {
    var request = new Request("SELECT * from customers ", function(err) {
      if (err) {
        console.log(err);
        callback(err, null);
      }
    });
  
    var resultArray = [];
  
    request.on('row', function(columns) {
      var rowObject = {};
      columns.forEach(function(column) {
        if (column.value === null) {
          rowObject[column.metadata.colName] = null;
        } else {
          rowObject[column.metadata.colName] = column.value;
        }
      });
      resultArray.push(rowObject);
    });
  
    // Close the connection after the final event emitted by the request, after the callback passes
    request.on("requestCompleted", function(rowCount, more) {
      //connection.close();
      callback(null, resultArray);
    });
  
    connection.execSql(request);
  }
module.exports = router;

