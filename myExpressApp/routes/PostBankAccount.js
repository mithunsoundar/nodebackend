// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());
const connection = require('./db');



//postapi for bankaccount
router.post('/bank/api/BankAccount', (req, res) => {
    var user=req.body;
    CreateBankAccount(user , function(err, result) {
      if (err) {
        console.error("Error creating BankAccount:", err);
      } else {
        console.log("BankAccount created successfully:", result);
      }
    });
    res.send("completed")
  });
//createBankAccount
  function CreateBankAccount(user , callback) {
    let sql = util.format("INSERT INTO BankAccount (account_no,customer_id,balance,account_status) VALUES (%s, %s, %s,'%s')",
      user.account_no,
      user.customer_id,
      user.balance,
      user.account_status
    );
    
      var request = new Request(sql, function(err, result) {
        // Handle the callback logic here
        callback(err, result);
      });
          // Replace it with the appropriate code for your database library
    request.on('row', function(columns) {
      console.log("selected  successfully");
    });
  
    request.on('rowsaffected', rowCount => {
      console.log("inserted successfully")
      });
      connection.execSql(request);
  }
module.exports = router;