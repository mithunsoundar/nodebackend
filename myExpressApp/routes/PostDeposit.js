// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const connection = require('./db');
const router = express.Router();
router.use(bodyParser.json());



//post Api for deposit money in bankaccount
router.post('/bank/api/bankaccount/Deposit', (req, res) => {
  var Depositdetails = req.body;  
  DepositMoney(Depositdetails.name, Depositdetails.amount, function(err, result) {
    if (err) {
      console.error("Error deposit money:", err);
      res.status(500).send("Error deposit money");
    } else {
      console.log("Money Deposit successfully:", result);
      // Respond once the transaction is completed
      res.send("Deposit completed");
    }
  });
});


function DepositMoney(customerName, amountToDeposit, callback) {
    // call UpdateTransaction Method 
    UpdateBankAccountTableDeposit(customerName, amountToDeposit, function(err, rowCount, rows) {
      if (err) {
        console.error('Error:', err);
        callback(err); // Pass the error to the callback
      } else {
        console.log('Rows affected:', rowCount);
        console.log('Selected rows:', rows);
        callback(null, "Transaction successful"); // Pass null for error (indicating success)
      }
    });
  } 
function UpdateBankAccountTableDeposit(customerName, amountToDeposit, callback)
{
  const sql = util.format(
    "UPDATE BankAccount SET balance = balance + %d WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = '%s')",
    amountToDeposit,
    customerName    
  );
  var request = new Request(sql, function(err, result) {
    // Handle the callback logic here
    callback(err, result);
  });
      // Replace it with the appropriate code for your database library
  request.on('row', function(columns) {
  console.log("selected  successfully");
  });
  
  request.on('rowsaffected', rowCount => {
  console.log("inserted successfully")
  });
  connection.execSql(request);
}  

module.exports = router;