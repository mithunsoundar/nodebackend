// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());
const connection = require('./db');


//post Api for withdraw in bankAccount
router.post('/bank/api/bankaccount/withdraw', (req, res) => {
    var withdrawdetails = req.body;  
    withdrawMoney(withdrawdetails.name, withdrawdetails.amount, function(err, result) {
      if (err) {
        console.error("Error withdrawing money:", err);
        res.status(500).send("Error withdrawing money");
      } else {
        console.log("Money withdrawn successfully:", result);
        // Respond once the transaction is completed
        res.send("Withdrawal completed");
      }
    });
  });

  function withdrawMoney(customerName, amountToWithdraw, callback) {
    // call UpdateTransaction Method 
    UpdateBankAccountTable(customerName, amountToWithdraw, function(err, rowCount, rows) {
      if (err) {
        console.error('Error:', err);
        callback(err); // Pass the error to the callback
      } else {
        console.log('Rows affected:', rowCount);
        console.log('Selected rows:', rows);
        callback(null, "Transaction successful"); // Pass null for error (indicating success)
      }
    });
  }

  
function UpdateBankAccountTable(customerName, amountToWithdraw, callback)
{
  const sql = util.format(
    "UPDATE BankAccount SET balance = balance - %d WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = '%s')",
    amountToWithdraw,
    customerName    
  );
  var request = new Request(sql, function(err, result) {
    // Handle the callback logic here
    callback(err, result);
  });
      // Replace it with the appropriate code for your database library
  request.on('row', function(columns) {
  console.log("selected  successfully");
  });
  
  request.on('rowsaffected', rowCount => {
  console.log("inserted successfully")
  });
  connection.execSql(request);
}
module.exports = router;
