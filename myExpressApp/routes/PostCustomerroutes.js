// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());
const connection = require('./db');




router.post('/bank/api/customers', (req, res) => {
    var customer=req.body;
    CreateCustomer(customer , function(err, result) {
      if (err) {
        console.error("Error creating customer:", err);
      } else {
        console.log("Customer created successfully:", result);
      }
    });
    res.send("complted")
  });
  function CreateCustomer(user , callback) {

    let sql = util.format(
      "INSERT INTO Customers (customer_id, name, phone, email, gender, address) VALUES (%s , '%s', '%s', '%s', '%s', '%s')",
      user.customer_id,
      user.name,
      user.phone,
      user.email,
      user.gender,
      user.address
    );  
    var request = new Request(sql, function(err, result) {
      // Handle the callback logic here
      callback(err, result);
    });
    // Replace it with the appropriate code for your database library
    request.on('row', function(columns) {
      console.log("selected  successfully");
    });
  
    request.on('rowsaffected', rowCount => {
      console.log("inserted successfully")
      });
      connection.execSql(request);
  }

  module.exports = router;