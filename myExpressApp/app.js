// app.js
const express = require('express');
const Generalroutes = require('./routes/routes');
const getBankAccountRoutes = require('./routes/GetBankAccount');
const getCustomerBasedOnIdRoutes = require('./routes/GetCustomerBasedonID');
const getCustomerRoutes = require('./routes/Getcustomerroutes');
const postBankAccountRoutes = require('./routes/PostBankAccount');
const postCustomerRoutes = require('./routes/PostCustomerroutes');
const postDepositRoutes = require('./routes/PostDeposit');
const postTransferRoutes = require('./routes/PostTransfer');
const postWithdrawRoutes = require('./routes/PostWithdraw');

// add routes based on below js files 



// TEST 
const app = express();
const port = 8080;

// Use the routes
app.use('/', Generalroutes);
// Use the routes
app.use('/', getBankAccountRoutes);
app.use('/', getCustomerBasedOnIdRoutes);
app.use('/', getCustomerRoutes);
app.use('/', postBankAccountRoutes);
app.use('/', postCustomerRoutes);
app.use('/', postDepositRoutes);
app.use('/', postTransferRoutes);
app.use('/', postWithdrawRoutes);



// Starting the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
