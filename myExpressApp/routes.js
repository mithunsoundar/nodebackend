// routes.js
const util = require('util');
 
const express = require('express');
const bodyParser = require('body-parser');
const Request = require('tedious').Request;
const router = express.Router();
router.use(bodyParser.json());


var Connection = require('tedious').Connection;  
var config = {  
  server: 'localhost',  //update me
  authentication: {
      type: 'default',
      options: {    
          userName: 'Aishu', //update me
          password: 'Aishu'  //update me              
      }
  },
  options: {
      // If you are on Microsoft Azure, you need encryption:          
      database: 'Banking',  //update me
      trustedConnection: true,
      encrypt: true,          
      trustServerCertificate: true,
      
  }
};  



var connection = new Connection(config);  
connection.on('connect', function(err) {  
  if (err) {
    console.error('Error connecting to the database:', err.message);
  } else {
    console.log('Connected to the database');
  }
});
connection.connect();
//Customers ApI
// Get method
router.get('/bank/api/customers', (req, res) => {
 
  GetCustomers((err, result) => {
    if (err) {
      console.error("Error:", err);
      res.status(500).send("Internal Server Error");
    } else {
      console.log("Result:", JSON.stringify(result, null, 2));
      // Send the JSON result as a response
      res.send(result);
    }
  });  
});
//BankAccount Api
// GET route to get bank account data by ID
router.get('/bank/api/bankaccount', (req, res) => {
  console.log("Default rount for bankaccount Success")
  GetBankAccount((err, result) => {
    if (err) {
      console.error("Error:", err);
      res.status(500).send("Internal Server Error");
    } else {
      console.log("Result:", JSON.stringify(result, null, 2));
      // Send the JSON result as a response
      res.send(result);
    }
  });  
  
});


//Customer Api
// Get Customer based on customer ID = 111

router.get('/bank/api/customer/:id', (req, res) => {
  const CustomerID = parseInt(req.params.id);
  console.log(CustomerID)
  //res.send(CustomerID.toString())
  GetCustomerBasedonID(CustomerID.toString(), function(err, result) {
    if (err) {
      console.error("Error:", err);
      res.status(500).send("Internal Server Error");
    } else {
      console.log("Result:", JSON.stringify(result, null, 2));
      // Send the JSON result as a response
      res.send(result);
    }
  });  

  
});





function GetCustomers(callback) {
  var request = new Request("SELECT * from customers ", function(err) {
    if (err) {
      console.log(err);
      callback(err, null);
    }
  });

  var resultArray = [];

  request.on('row', function(columns) {
    var rowObject = {};
    columns.forEach(function(column) {
      if (column.value === null) {
        rowObject[column.metadata.colName] = null;
      } else {
        rowObject[column.metadata.colName] = column.value;
      }
    });
    resultArray.push(rowObject);
  });

  // Close the connection after the final event emitted by the request, after the callback passes
  request.on("requestCompleted", function(rowCount, more) {
    //connection.close();
    callback(null, resultArray);
  });

  connection.execSql(request);
}
function GetCustomerBasedonID(CustomerID,callback) {

  let query = util.format('SELECT * from customers Where customer_id = %s', CustomerID);

  var request = new Request(query, function(err) {
    if (err) {
      console.log(err);
      callback(err, null);
    }
  });

  var resultArray = [];

  request.on('row', function(columns) {
    var rowObject = {};
    columns.forEach(function(column) {
      if (column.value === null) {
        rowObject[column.metadata.colName] = null;
      } else {
        rowObject[column.metadata.colName] = column.value;
      }
    });
    resultArray.push(rowObject);
  });

  // Close the connection after the final event emitted by the request, after the callback passes
  request.on("requestCompleted", function(rowCount, more) {
    //connection.close();
    callback(null, resultArray);
  });

  connection.execSql(request);
}


function GetBankAccount(callback) {
  var request = new Request("Select * from BankAccount ", function(err) {
    if (err) {
      console.log(err);
 
      callback(err, null);
    }
  });

  var resultArray = [];

  request.on('row', function(columns) {
    var rowObject = {};
    columns.forEach(function(column) {
      if (column.value === null) {
        rowObject[column.metadata.colName] = null;
      } else {
        rowObject[column.metadata.colName] = column.value;
      }
    });
    resultArray.push(rowObject);
  });

  // Close the connection after the final event emitted by the request, after the callback passes
  request.on("requestCompleted", function(rowCount, more) {
    //connection.close();
    callback(null, resultArray);
  });

  connection.execSql(request);
}


router.get('/', (req, res) => {
  res.send("Default route for /");
});

router.get('/api/employee', (req, res) => {
  res.send("Default route for / employee");
})

router.get('/api/users', (req, res) => {
  res.send("route for /api/users");
  //res.json(users);
});

router.get('/api/apartment', (req, res) => {
  res.send("Default route for /apartment");
})

router.get('/api/company', (req, res) => {
  res.send("route for /api/company");
})


// Set user name
router.get('/api/book', (req, res) => {
  res.send("route for /api/book");
});


//put id
router.put('/api/book/:id', (req, res) => {
  const userId = parseInt(req.params.id);
  console.log(userId)
  res.send("Success")
});

//put id
router.put('/api/company/:id', (req, res) => {
  const userId = parseInt(req.params.id);
  console.log(userId)
  res.send("Success")
});

router.put('/api/apartment/:id', (req, res) => {
  const userId = parseInt(req.params.id);
  console.log(userId)
  res.send("Success")
});
//put username id age
router.put('/api/set-user/:id/:username/:age', (req, res) => {
  const userId = parseInt(req.params.id);
  const newUsername = req.params.username;  // Corrected from req.params.name
  const newAge = parseInt(req.params.age);
  
  console.log(userId + " " + newUsername + " " + newAge);  // Corrected from id, username, age
  res.send("Success");
});

 
module.exports = router;
